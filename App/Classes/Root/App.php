<?php

namespace App\Classes\Root;

use App\Classes\Coin\WalletCoin;
use App\Classes\Wallet\Wallet;
use App\Classes\Wallet\WalletObserver;

final class App {

    private $wallet;

    public function __construct()
    {
        $this->wallet = new Wallet();
        $this->wallet->attach(new WalletObserver());
    }
    
    public function run()
    {
        $walletCoin = new WalletCoin(5);
        $this->wallet->addCoin($walletCoin);
        $this->wallet->addCoin($walletCoin);
        $this->wallet->addCoin($walletCoin);
        $this->wallet->addCoin($walletCoin);
        $this->wallet->addCoin($walletCoin);

        return [
            'addActions' => $this->wallet->getAddActionsCount(),
            'deleteActions' => $this->wallet->getDeleteActionsCount(),
            'promoCoins' => $this->wallet->getPromoCoinState(),
            'walletCoins' => $this->wallet->getWalletCoinState()
        ];
    }

}