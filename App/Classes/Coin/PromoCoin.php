<?php

namespace App\Classes\Coin;

use DateTime;
use App\Classes\Coin\Coin;

class PromoCoin extends Coin {
    const TYPE = 'PROMO_COIN';
    private $id;
    private $value;
    private $validTo;

    public function __construct(int $value, DateTime $validTo = null)
    {
        $this->id = uniqid();
        $this->value = $value;
        $this->validTo = $validTo;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getValue(): int
    {
        return $this->value;
    }

    public function getValidTo(): DateTime
    {
        return $this->validTo;
    }

    public function __toString(): string
    {
        return self::TYPE;
    }

} 