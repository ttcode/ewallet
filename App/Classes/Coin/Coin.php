<?php

namespace App\Classes\Coin;

use DateTime;

abstract class Coin {
    abstract public function getId(): string;
    abstract public function getValue(): int;
    abstract public function getValidTo(): DateTime;
}