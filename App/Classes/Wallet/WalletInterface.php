<?php

namespace App\Classes\Wallet;

use App\Classes\Coin\Coin;
use App\Classes\Coin\PromoCoin;

interface WalletInterface {
    public function addCoin(Coin $coin): Coin;
    public function deleteCoin(Coin $coin): void;
    public function addGratisCoin(PromoCoin $promoCoin): void;
    public function getState(): array;
    public function getPromoCoinState(): array;
    public function getWalletCoinState(): array;
    public function getAddActionsCount(): int;
    public function getDeleteActionsCount(): int;
    public function getWalletCoinsCount(): int;
    public function getPromoCoinsCount(): int;
}