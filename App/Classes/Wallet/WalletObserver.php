<?php

namespace App\Classes\Wallet;

use SplObserver;
use SplSubject;

class WalletObserver implements SplObserver {
    
    private $changedStates = [];
    private $addActionsCount = 0;
    private $deleteActionsCount = 0;

    public function update(SplSubject $subject): void
    {
        $this->changedStates[] = clone $subject;
        $this->calculateActions();
    }

    public function getCurrentActions(): array {
        return $this->changedStates;
    }

    public function getCurrentActionsAddCount(): int {
        return $this->addActionsCount;
    }

    public function getCurrentActionsDeleteCount(): int {
        return $this->deleteActionsCount;
    }

    private function calculateActions(): void {
        $lastActionsState = 0;
        foreach ($this->changedStates as $wallet) {
            if ($wallet->getWalletCoinsCount() > $lastActionsState) {
                $this->addActionsCount++;
            }
            if ($wallet->getWalletCoinsCount() < $lastActionsState) {
                $this->deleteActionsCount++;
            }
            $lastActionsState = $wallet->getWalletCoinsCount();
        }
    }

}