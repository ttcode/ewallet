<?php

namespace App\Classes\Wallet;

use App\Classes\Coin\Coin;
use App\Classes\Coin\PromoCoin;
use App\Classes\PromoRule\PromoRuleResolver;
use SplObjectStorage;
use SplObserver;
use SplSubject;

class Wallet implements WalletInterface, SplSubject {

    private $state = [];
    private $actionsObservers = [];
    private $promoRuleResolver;

    public function __construct()
    {
        $this->actionsObservers = new SplObjectStorage();
        $this->promoRuleResolver = new PromoRuleResolver();
    }

    public function attach(SplObserver $observer)
    {
        $this->actionsObservers->attach($observer);
    }

    public function detach(SplObserver $observer)
    {
        $this->actionsObservers->detach($observer);
    }

    public function notify()
    {
        foreach ($this->actionsObservers as $observer) {
            $observer->update($this);
        }

        $this->promoRuleResolver->resolve($this);
    }

    public function addCoin(Coin $coin): Coin
    {
        $this->state[$coin->getId()] = $coin;
        $this->notify();
        return $coin;
    }

    public function deleteCoin(Coin $coin): void
    {
        unset($this->state[$coin->getId()]);
        $this->notify();
    }

    public function getState(): array
    {
        return $this->state;
    }

    public function getWalletCoinState(): array
    {
        return array_filter($this->state, function($coin) {
            return $coin::TYPE === 'WALLET_COIN';
        });
    }

    public function getPromoCoinState(): array
    {
        return array_filter($this->state, function($coin) {
            return $coin::TYPE === 'PROMO_COIN';
        });
    }

    public function getAddActionsCount(): int {
        $cnt = 0;
        foreach ($this->actionsObservers as $observer) {
            if ($observer instanceof WalletObserver) {
                $cnt = $observer->getCurrentActionsAddCount();
            }
        }
        return $cnt;
    }

    public function getDeleteActionsCount(): int {
        $cnt = 0;
        foreach ($this->actionsObservers as $observer) {
            if ($observer instanceof WalletObserver) {
                $cnt = $observer->getCurrentActionsDeleteCount();
            }
        }
        return $cnt;
    }

    public function addGratisCoin(PromoCoin $promoCoin): void {
        $this->state[$promoCoin->getId()] = $promoCoin;
    }

    public function getWalletCoinsCount(): int {
        return count($this->getWalletCoinState());
    }

    public function getPromoCoinsCount(): int {
        return count($this->getPromoCoinState());
    }

}