<?php

namespace App\Classes\PromoRule;

use App\Classes\Coin\PromoCoin;
use App\Classes\Wallet\WalletInterface;
use DateTime;

class PromoRule20 implements PromoRuleInterface {

    const VALID_DAYS = 3;
    const MIN_COUNT_ADD = 1;
    const MIN_COUNT_DELETE = 9;
    const SUM_DELETE = 100;
    const PROMO_COINS_LOAD = 20;

    public function applyPromotion(WalletInterface $wallet): void
    {
        if ($this->calculateRule($wallet)) {
            $validDate = new DateTime();
            $validDate->modify('+' . self::VALID_DAYS . ' days');
            for ($i = 0; $i <= self::PROMO_COINS_LOAD; $i++) {
                $promoCoin = new PromoCoin(1, $validDate);
                $wallet->addGratisCoin($promoCoin);
            }
        }
    }

    private function calculateRule(WalletInterface $wallet): bool
    {
        if ($wallet->getDeleteActionsCount() === self::MIN_COUNT_DELETE
            && $wallet->getAddActionsCount() === self::MIN_COUNT_ADD) {
            return true;
        }
        return false;
    }

}