<?php

namespace App\Classes\PromoRule;

use App\Classes\Wallet\WalletInterface;

interface PromoRuleResolverInterface {
    public function getEnabledRules(): array;
    public function resolve(WalletInterface $wallet): void;
}