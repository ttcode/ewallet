<?php

namespace App\Classes\PromoRule;

use App\Classes\Wallet\WalletInterface;
use App\Config\Config;

class PromoRuleResolver implements PromoRuleResolverInterface {

    private $enabledRules = [];

    public function __construct()
    {
        $this->registerEnabledRules();
    }

    public function getEnabledRules(): array {
        return $this->enabledRules;
    }

    public function resolve(WalletInterface $wallet): void {
        foreach ($this->enabledRules as $rule) {
            $rule->applyPromotion($wallet);
        }
    }

    protected function registerEnabledRules(): void {
        $rulesInConfig = Config::getConfigInstance()->checkEnabled('promoRules');
        foreach ($rulesInConfig as $ruleName => $isEnabled) {
            if ($isEnabled === true) {
                $className = "App\\Classes\\PromoRule\\" . $ruleName;
                $this->enabledRules[] = new $className();
            }
        }
    }

}