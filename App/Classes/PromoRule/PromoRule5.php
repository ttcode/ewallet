<?php

namespace App\Classes\PromoRule;

use App\Classes\Coin\PromoCoin;
use App\Classes\Wallet\WalletInterface;
use DateTime;

class PromoRule5 implements PromoRuleInterface {
    
    const VALID_DAYS = 5;
    const MIN_COUNT = 5;
    const PROMO_COINS_LOAD = 10;

    public function applyPromotion(WalletInterface $wallet): void
    {
        if ($this->calculateRule($wallet)) {
            $validDate = new DateTime();
            $validDate->modify('+' . self::VALID_DAYS . ' days');
            for ($i = 0; $i <= self::PROMO_COINS_LOAD; $i++) {
                $promoCoin = new PromoCoin(1, $validDate);
                $wallet->addGratisCoin($promoCoin);
            }
        }
    }

    private function calculateRule(WalletInterface $wallet): bool
    {
        if ($wallet->getAddActionsCount() === self::MIN_COUNT) {
            return true;
        }
        return false;
    }

}