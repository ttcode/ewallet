<?php

namespace App\Classes\PromoRule;

use App\Classes\Coin\PromoCoin;
use App\Classes\PromoRule\PromoRuleInterface;
use App\Classes\Wallet\WalletInterface;

class PromoRuleInitial implements PromoRuleInterface {

    const MIN_COUNT = 1;
    const PROMO_COINS_LOAD = 30;

    public function applyPromotion(WalletInterface $wallet): void
    {
        if ($this->calculateRule($wallet)) {
            for ($i = 0; $i < self::PROMO_COINS_LOAD; $i++) {
                $promoCoin = new PromoCoin(1);
                $wallet->addGratisCoin($promoCoin);
            }
        }
    }

    private function calculateRule(WalletInterface $wallet): bool
    {
        if ($wallet->getAddActionsCount() === self::MIN_COUNT) {
            return true;
        }
        return false;
    }

}