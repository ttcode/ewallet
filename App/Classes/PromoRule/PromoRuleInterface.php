<?php 

namespace App\Classes\PromoRule;

use App\Classes\Wallet\WalletInterface;

interface PromoRuleInterface {
    public function applyPromotion(WalletInterface $wallet): void;
}