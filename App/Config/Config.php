<?php

namespace App\Config;

use App\Exceptions\FileNotFoundException;

final class Config implements ConfigInterface {

    const CONFIG_FILE = __DIR__ . '/../../Config/app_config.json';
    private static $instance = null;
    private $configContent = [];
    
    public function checkEnabled(string $key)
    {
        if (isset($this->configContent[$key])) {
            return $this->configContent[$key];
        }        
        return false;
    }

    public static function getConfigInstance(): self {
        if (static::$instance === null) {
            static::$instance = new static();
        }
        return static::$instance;
    }

    private function checkConfigFile(): void {
        if (!file_exists(self::CONFIG_FILE)) {
            throw new FileNotFoundException("Config file not found", 404);
        }
    }

    private function parseFile(): void {
        $this->configContent = json_decode(file_get_contents(self::CONFIG_FILE), true);
    }

    private function __construct()
    {
        $this->checkConfigFile();
        $this->parseFile();
    }

    private function __clone()
    {

    }

    private function __wakeup()
    {

    }
}