<?php

namespace App\Config;

interface ConfigInterface {
    public function checkEnabled(string $key);
}