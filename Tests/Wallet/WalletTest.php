<?php

use App\Classes\Coin\PromoCoin;
use App\Classes\Coin\WalletCoin;
use App\Classes\Wallet\Wallet;
use PHPUnit\Framework\TestCase;

class WalletTest extends TestCase {

    public function testWalletCoinStateContainsOnlyWalletCoins() {
        $wallet = new Wallet();
        $walletCoin = new WalletCoin(5);
        $promoCoin = new PromoCoin(10, new DateTime());
        $wallet->addCoin($walletCoin);
        $wallet->addCoin($promoCoin);
        $this->assertArrayNotHasKey($promoCoin->getId(), $wallet->getWalletCoinState());
    }

    public function testPromoCoinStateContainsOnlyPromoCoins() {
        $wallet = new Wallet();
        $walletCoin = new WalletCoin(5);
        $promoCoin = new PromoCoin(10, new DateTime());
        $wallet->addCoin($walletCoin);
        $wallet->addCoin($promoCoin);
        $this->assertArrayNotHasKey($walletCoin->getId(), $wallet->getPromoCoinState());
    }

}